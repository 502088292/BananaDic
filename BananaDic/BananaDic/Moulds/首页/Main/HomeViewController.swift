//
//  HomeViewController.swift
//  BananaDic
//
//  Created by banana on 2/4/16.
//  Copyright © 2016 banana. All rights reserved.
//

import UIKit


class HomeViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let instance = SomeClass()
        instance.doSomething()
        print(instance.x)
        
        
        completionHandlers.first?()
        print(instance.x)
    }
    


    @IBAction func translateClick(sender: UIBarButtonItem) {

        NetworkDataCenter().getHomeWordTranslation("request", target: self, callBack: "result")
    }
    
    func result(dic:NSDictionary){
        print("执行了")
    }
    
    @IBAction func click(sender: UIBarButtonItem) {

    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

    }
}

func someFunctionWithNoescapeClosure(@noescape closure: () -> Void) {
    closure()
}

var completionHandlers: [() -> Void] = []
func someFunctionWithEscapingClosure(completionHandler: () -> Void) {
    completionHandlers.append(completionHandler)
}

class SomeClass{
    var x = 10
    func doSomething() {
        
        someFunctionWithEscapingClosure { self.x = 100 }
        someFunctionWithNoescapeClosure { x = 200 }
        
        
    }
}

