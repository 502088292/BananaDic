//
//  Home+NetworkDataCenter.swift
//  BananaDic
//
//  Created by banana on 2/15/16.
//  Copyright © 2016 banana. All rights reserved.
//

import UIKit

extension NetworkDataCenter {
    /**
     * 单词翻译
     * @parametners word: 要翻译的单词
     */
    func getHomeWordTranslation(word: String, target:AnyObject, callBack:Selector){
        NetworkDataCenter.POST("http://dict.youdao.com/jsonapi?", Parameters: ["q":word,"doctype":"json","keyfrom":"mac.main","id":"1D767807C45AF099198760189FE7DCB3","vendor":"appstore","appVer":"1.4","client":"macdict","jsonversion": "2"],target: target ,callBack: callBack)
    }
    
}

