//
//  HttpRequest.swift
//  BananaDic
//
//  Created by banana on 2/15/16.
//  Copyright © 2016 banana. All rights reserved.
//

import UIKit
import Alamofire

class HttpRequest: NSObject {
    
    class func shareInstance()->HttpRequest{
        
        struct YRSingleton{
            static var predicate:dispatch_once_t = 0
            static var instance:HttpRequest? = nil
        }
        dispatch_once(&YRSingleton.predicate,{
            YRSingleton.instance=HttpRequest()
            }
        )
        return YRSingleton.instance!
    }

    static func POST(path: String, Parameters: NSDictionary, target:AnyObject, callBack:Selector){
        Alamofire.request(.POST, path, parameters: Parameters as? [String : AnyObject])
            .responseJSON { response in
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                }
        }
    }
    
    static func POSTa(path: String, Parameters: NSDictionary, target:AnyObject, callBack:Selector) -> Bool{
        
        Alamofire.request(.POST, path, parameters: Parameters as? [String : AnyObject])
            .responseJSON { response in
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")

                }else {

                }

        }
        return true
    }
}
