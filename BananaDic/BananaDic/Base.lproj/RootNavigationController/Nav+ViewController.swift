//
//  Nav+ViewController.swift
//  BananaDic
//
//  Created by banana on 2/15/16.
//  Copyright © 2016 banana. All rights reserved.
//

import UIKit


extension UIViewController {
    
    /**
     * 跳往首页
     */
    func pushHomeViewController(){
        let mainStoryboard = UIStoryboard(name: "HomeViewController", bundle: NSBundle.mainBundle())
        let vc : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("HomeViewController")
        vc.navigationItem.hidesBackButton = true
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    /**
     * 视图跳转
     * @param name, storyboardName与viewControllerWithIdentifier.
     */
    func pushViewController(name:String) {
        let mainStoryboard = UIStoryboard(name: name, bundle: NSBundle.mainBundle())
        let vc : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier(name)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

